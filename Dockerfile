FROM eclipse-temurin:17-jre

ARG version="2.4.31"

RUN curl -o /usr/local/src/swagger-codegen-cli.jar \
    https://repo1.maven.org/maven2/io/swagger/swagger-codegen-cli/${version}/swagger-codegen-cli-${version}.jar

ENTRYPOINT ["java", "-jar", "/usr/local/src/swagger-codegen-cli.jar"]

WORKDIR /data

VOLUME /data

RUN chmod 0777 .
